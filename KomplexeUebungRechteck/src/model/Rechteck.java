package model;
import java.util.Random;
public class Rechteck {
private int breite, hoehe;
private Punkt p = new Punkt();
public Rechteck() {
	super();
}

public Rechteck(int x, int y, int breite, int hoehe) {
	super();
	this.p.setX(x);
	this.p.setY(y);
	this.setBreite(breite);;
	this.setHoehe(hoehe);
}

public int getX() {
	return p.getX();
}

public void setX(int x) {
	this.p.setX(x);
}

public int getY() {
	return p.getY();
}

public void setY(int y) {
	this.p.setY(y);
}

public int getBreite() {
	return breite;
}

public void setBreite(int breite) {
	this.breite = Math.abs(breite);
}

public int getHoehe() {
	return hoehe;
}

public void setHoehe(int hoehe) {
	this.hoehe = Math.abs(hoehe);
}
public boolean enthaelt(int x, int y) {
if(this.p.getX() == x && this.p.getY() == y)
return true;	
return false;	
}

public boolean enthaelt(Punkt p) {

return p.equals(p);	
}

public boolean enthaelt(Rechteck rechteck) {
if(this.getX() <= rechteck.getX() && this.getBreite() >= (rechteck.getBreite() + rechteck.getX()) 
	&& this.getY() <= rechteck.getY() && this.getHoehe() >= (rechteck.getHoehe() + rechteck.getY()))
	return true;
return false;	
}

@Override
public String toString() {
	return "Rechteck [x=" + p.getX() + ", y=" + p.getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
}

public static Rechteck generiereZufallsRechteck() {
Random r = new Random();
int x = r.nextInt(1200);
int y = r.nextInt(1000);
int breite = r.nextInt(1200 - x);
int hoehe = r.nextInt(1000 - y);
Rechteck rechteck = new Rechteck(x,y,breite,hoehe);
return rechteck;	
}
}
