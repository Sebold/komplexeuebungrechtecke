package model;

public class Punkt {
private int x,y;

public Punkt() {
	
}
public Punkt(int x,int y) {
this.x = x;
this.y = y;
}
public int getX() {
	return x;
}
public void setX(int x) {
	this.x = x;
}
public int getY() {
	return y;
}
public void setY(int y) {
	this.y = y;
}
public boolean equals(Punkt p) {
if(this.x == p.getX() && this.y == p.getY())
return true;	
return false;	
}
@Override
public String toString() {
	return "Punkt [x=" + x + ", y=" + y + "]";
}
}
