package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import controller.BunteRechteckeController;

public class Zeichenflaeche extends JPanel {
	BunteRechteckeController brc;
	/**
	 * Create the panel.
	 */
	public Zeichenflaeche(BunteRechteckeController brc) {
   super();
   this.brc = brc;
	}
@Override
public void paintComponent(Graphics g) {
 
g.setColor(Color.BLACK);

g.drawRect(0, 0, 50, 50);
for(int i = 0; i< brc.getRechtecke().size(); i++) {
g.drawRect(brc.getRechtecke().get(i).getX(),brc.getRechtecke().get(i).getY(),brc.getRechtecke().get(i).getBreite() ,brc.getRechtecke().get(i).getHoehe());
}
}
}
