package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import controller.BunteRechteckeController;
import model.Rechteck;

public class Eingabemaske {

	private JFrame frame;
	private JTextField tfd_xValue;
	private JTextField tfd_yValue;
	private JTextField tfd_widthValue;
	private JTextField tfd_heightValue;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Eingabemaske window = new Eingabemaske();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Eingabemaske() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lbl_headline = new JLabel("Ein Rechteck Hinzuf\u00FCgen");
		lbl_headline.setFont(new Font("Comic Sans MS", Font.PLAIN, 26));
		lbl_headline.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lbl_headline, BorderLayout.NORTH);
		
		JPanel pnl_grid_input = new JPanel();
		frame.getContentPane().add(pnl_grid_input, BorderLayout.CENTER);
		pnl_grid_input.setLayout(new GridLayout(4, 2, 0, 0));
		
		JLabel lbl_xValue = new JLabel("X-Wert :");
		lbl_xValue.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		lbl_xValue.setHorizontalAlignment(SwingConstants.CENTER);
		pnl_grid_input.add(lbl_xValue);
		
		tfd_xValue = new JTextField();
		pnl_grid_input.add(tfd_xValue);
		tfd_xValue.setColumns(10);
		
		JLabel lbl_yValue = new JLabel("Y-Wert :");
		lbl_yValue.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_yValue.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		pnl_grid_input.add(lbl_yValue);
		
		tfd_yValue = new JTextField();
		pnl_grid_input.add(tfd_yValue);
		tfd_yValue.setColumns(10);
		
		JLabel lbl_widthValue = new JLabel("Breitenwert : ");
		lbl_widthValue.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		lbl_widthValue.setHorizontalAlignment(SwingConstants.CENTER);
		pnl_grid_input.add(lbl_widthValue);
		
		tfd_widthValue = new JTextField();
		pnl_grid_input.add(tfd_widthValue);
		tfd_widthValue.setColumns(10);
		
		JLabel lbl_heightValue = new JLabel("H\u00F6henwert : ");
		lbl_heightValue.setFont(new Font("Comic Sans MS", Font.PLAIN, 16));
		lbl_heightValue.setHorizontalAlignment(SwingConstants.CENTER);
		pnl_grid_input.add(lbl_heightValue);
		
		tfd_heightValue = new JTextField();
		pnl_grid_input.add(tfd_heightValue);
		tfd_heightValue.setColumns(10);
		
		JButton btn_input = new JButton("Einf\u00FCgen");
		btn_input.setFont(new Font("Comic Sans MS", Font.PLAIN, 18));
		btn_input.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			BunteRechteckeController brc = new BunteRechteckeController();
			Rechteck r = new Rechteck(Integer.parseInt(tfd_xValue.getText()),Integer.parseInt(tfd_yValue.getText()),Integer.parseInt(tfd_widthValue.getText()),Integer.parseInt(tfd_heightValue.getText()));
			brc.add(r);
			tfd_xValue.setText(brc.getRechtecke().toString());
			}
		});
		frame.getContentPane().add(btn_input, BorderLayout.SOUTH);
	}

}
